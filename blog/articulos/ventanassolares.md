# LA ESTÉTICA EN LA ENERGÍA FOTOVOLTAICA

Desde [**placas solares**](https://www.cambioenergetico.com/171-placas-solares) de colores a tejas fotovoltaicas o “ventanas solares”. A medida que la energía solar se populariza entre el público, el aspecto estético de las
instalaciones fotovoltaicas y su capacidad de integración en la arquitectura de los
inmuebles van tomando mayor relevancia como factores de compra. La cuestión
que hay que plantearse es si todos estos desarrollos en pro de un diseño más
estético pasan la auténtica prueba de fuego de cualquier tecnología fotovoltaica: la
eficiencia.

## Cuestión de estrategia
El objetivo de producir **instalaciones fotovoltaicas** cada vez más estéticas e integradas en
residencias y edificios ha hecho aparecer un nuevo escenario en el que fabricantes de
equipos de todo el mundo están desplegando sus mejores armas. Se trata de una suerte de
“batalla por la belleza fotovoltaica” en la que se vislumbran varias estrategias
fundamentales.
La primera de ellas la ponen en práctica los fabricantes de componentes (baterías, placas
solares, etc.) que ponen cada vez más cuidado en el aspecto visual de sus productos: desde
baterías que más bien parecen elementos de decoración interior
a *paneles solares sin marco*, multicolores o incluso decorados con imprintaciones
digitales. En el caso de las baterías, el diseño no parece estar reñido con la eficiencia. Es
más, los nuevos modelos de baterías de litio, además de un diseño mucho más estético,
están demostrando un rendimiento mucho mejor que las tradicionales baterías de plomo.
No ocurre así con los **paneles solares**. Al menos por ahora, someter a un panel solar a un
proceso de coloreado o de imprintación de cerámica supone restarle eficiencia y añadir un
coste adicional. En otras palabras, que se hace realidad el viejo dicho de que “para
presumir, hay que sufrir”.
Otros fabricantes están optando por una estrategia distinta: la de innovar, no tanto en los
componentes sino en la estructura de las instalaciones. Y es que no puede negarse que
existe un número de clientes interesados en la energía fotovoltaica pero a quienes no atrae
la idea de alterar el aspecto de sus inmuebles cubriendo el tejado con una estructura
metálica de gran tamaño que destaca sobre cualquier otro elemento arquitectónico. Para
ellos, algunos fabricantes han puesto en el mercado una oferta de instalaciones que se
ajustan perfectamente a la medida de cada cubierta particular.
Por lo general, estos fabricantes ofrecen una estructura –casi siempre coplanar- de placas
solares acompañadas de accesorios “embellecedores” fabricados en distintos materiales.
Estos accesorios no son generadores de energía fotovoltaica per se pero contribuyen a
integrar la instalación fotovoltaica en la cubierta del inmueble, obteniendo así un alto
grado de customización de la instalación.
En principio, este tipo de soluciones ofrecen un excelente aspecto visual sin afectar al
rendimiento del sistema. Eso sí, el hecho de contar con elementos adicionales para
embellecer la instalación unido a la dificultad que implica diseñar e instalar una estructura
de tejado específica probablemente supondrá un incremento en la inversión.

## La energía fotovoltaica como material de construcción
Finalmente, existe una tercera estrategia en la que nos vamos a detener un poco más y que
consiste en usar la tecnología fotovoltaica como material de construcción, ofreciendo
componentes arquitectónicos hechos con placas solares. En otras palabras: integración
total.
Todos conocemos el ejemplo de cubiertas para porche o cocheras
https://www.cambioenergetico.com/191-aparcamiento-solar hechas con paneles solares,
un tipo de desarrollo que está ganando en eficiencia merced a la aparición de tecnologías
como los paneles bifaciales (link a artículo de últimas tecnologías). Sin embargo,
difícilmente puede hablarse en este caso de material fotovoltaico de construcción, sino
más bien de un uso inteligente de la disposición de placas solares convencionales.
Un ejemplo que probablemente refleja mejor esta estrategia de utilizar la fotovoltaica
como material de construcción es el de las tejas solares,
https://www.cambioenergetico.com/paneles-solares/2550-tejas-solares-con-
cobertura.html que entraron con fuerza en el mercado fotovoltaico hace algunos años.
Podría decirse que las tejas solares son placas solares en miniatura, dado que funcionan
como cualquier panel solar y, de hecho, van conectadas al inversor de la misma manera.
La composición básica de una teja solar consiste en un raíl de aluminio, la pieza de teja y
finalmente la célula fotovoltaica, que puede llevar o no una cobertura de protección. El
material de las tejas es el ASA (acrilonitrilo estireno acrilato, muy utilizado en la industria
naval) lo que las dota de una excelente resistencia al mal tiempo, a los cambios de
temperatura y a otras amenazas como el salitre marino propio de los edificios cercanos a
la costa.
(ponemos imagen de aquí?: https://www.cambioenergetico.com/blog/tejas-solares-si-o-
no/#.XKNHtoUfUTk)
Lo mejor: la estética. Las tejas ofrecen una excelente capacidad de integración en todo
tipo de tejado, sobre el que pasan virtualmente inadvertidas. Tienen, además, una
excelente durabilidad, especialmente si cuentan con cobertura de protección.
Lo peor: la limitación más importante de la teja solar es la baja eficiencia que ofrece.
Mientras que solo se necesita un pequeño string de 3 o 4 paneles solares para producir
1kW de energía solar, se necesitan entre 9 y 11m2 de teja fotovoltaica –dependiendo de si
cuentan o no con cobertura- para generar la misma energía. El resultado es buena
integración y buena estética pero un precio más caro en comparación con las placas
solares convencionales y menor eficiencia que éstas. Le toca al consumidor decidir.

## Ventanas solares
Junto a las **tejas solares**, otro material de construcción ya en el mercado y con un gran
potencial estético y de integración en todo tipo de estructuras arquitectónicas es el vidrio
solar o, como gustan de llamarlo algunos fabricantes, la “*ventana solar*”.
Vaya por delante que, si bien ya es posible hoy en día adquirir en el mercado vidrio con
capacidad de generar energía solar –siendo una de las empresas mundiales más punteras,
precisamente, la española Onyx Solar- se trata de un desarrollo que aún cuenta con un
número de limitaciones y, por tanto, merece ser recibido con reservas.
Lo mejor: entre las cualidades del vidrio fotovoltaico que lo hacen un producto
potencialmente muy interesante destaca una… ¡que es vidrio! Las posibilidades de un
producto hecho de materia translúcida y con capacidad de generar energía fotovoltaica
hacen volar la imaginación: dispositivos electrónicos que se cargan al sol, vehículos
eléctricos que se cargan a través de sus lunas… y, por supuesto, ventanas.
El vidrio fotovoltaico es, además, un producto resistente. Cada módulo está construido con
capas de vidrio laminado de seguridad que encapsulan una capa delgada de células solares
de silicio amorfo, que es lo que le proporciona propiedades fotovoltaicas. Las pruebas de
ciclo de temperatura, humedad o carga mecánica realizadas hasta ahora arrojan buenos
resultados, como también los arrojan su capacidad de bloquear de manera eficiente la
transferencia de calor y de filtrar la radiación UV, ayudando así a prevenir el
sobrecalentamiento en el interior de los edificios.
Si a estas ventajas le añadimos otras características, como que existe una gama completa
de colores para los módulos o que es un material que puede cortarse en obra para
ajustarlo a cualquier superficie, el resultado es un producto de gran potencial e innegable
estética.
Lo peor: como ocurría con las *tejas solares*, la limitación más importante del vidrio
fotovoltaico reside en la escasa eficiencia de sus módulos. Aunque existen variaciones
dependiendo del color del vidrio y de si éste es opaco, translúcido o semi-transparente, los
paneles de vidrio fotovoltaico difícilmente ofrecen potencias pico de más de 50W (cristal
opaco) o 30W (translúcido) por m2. Se trata de unas cifras significativamente inferiores a
las que ofrece un [**panel solar**](https://www.cambioenergetico.com/26-paneles-solares) estándar con base de silicio.

Como consecuencia de lo anterior, las aplicaciones de los módulos de vidrio fotovoltaico se
reducen, dado que es necesario literalmente “envolver” el edificio con módulos para
obtener una potencia fotovoltaica considerable. No en vano, la mayoría de los proyectos
llevados a cabo con esta tecnología hasta ahora suelen limitarse a edificios de oficinas o
proyectos encargados por empresas y corporaciones con un interés específico por
proyectar una imagen de marca innovadora y un presupuesto holgado que permite
priorizar estética sobre eficiencia. Es precisamente al hablar de presupuesto donde
aparece otra limitación importante del vidrio fotovoltaico: su precio, significativamente
más elevado que el de las instalaciones fotovoltaicas de paneles solares opacos.

## La cuestión entonces es: ¿estética o ahorro energético?
Lamentablemente, sí. A día de hoy, el consumidor está obligado a elegir. A ver, no hay duda
de que todos los desarrollos en pro de la estética y la integración que hemos visto son
relevantes para el consumidor y están cambiando ya la manera de plantear las
instalaciones solares. Es más, están empezando a vislumbrarse avances mucho más
revolucionarios si cabe, como la producción de materiales fotovoltaicos hechos a base de
materiales orgánicos, (link a artículo de últimos avances en fotovoltaica), cuyas
aplicaciones prácticas y estéticas son potencialmente increíbles. Merece, pues, la pena
estar atentos a todos estos desarrollos porque es más que probable que se impongan en el
futuro.

Sin embargo, no debemos olvidar que, más que cualquier otra cosa, la decisión de instalar
energía solar es, en la mayoría de los casos, una decisión sobre sostenibilidad y eficiencia
energética y ahí, de momento, la mayoría de los productos más estéticos no están dando la
talla. Con esto en mente, parece claro que cubrir la cubierta de una residencia, un edificio o
una nave con una buena instalación de placas solares bien diseñada sigue siendo la mejor
opción en términos de coste-beneficio.
A medida que el mercado de la energía fotovoltaica sigue introduciendo novedades,
aumentan las razones para que los consumidores nos planteemos con seriedad la
transición a la energía solar fotovoltaica. Si estás pensando en dar el paso y necesitas
ayuda, contacta con Cambio Energético y te asesoraremos sin compromiso sobre los
beneficios de este modelo energético y sobre cómo hacer la transición al mismo de forma
ordenada, sencilla y al mínimo coste.
www.cambioenergetico.com