# BALANCE NETO Y AUTOCONSUMO FOTOVOLTAICO

Ofrecer al consumidor la posibilidad de recibir compensación por el excedente de la energía fotovoltaica que produzcan sus [**placas solares**](https://www.cambioenergetico.com/171-placas-solares) le ayudará a recuperar antes la inversión en su instalación de autoconsumo fotovoltaico.

## EL BALANCE NETO

Como ya hemos visto en posts anteriores, la introducción del [*Real Decreto-Ley 15/2018*](https://www.cambioenergetico.com/blog/nuevo-real-decreto-ley-15-2018-analisis-en-detalle/), de medidas urgentes para la transición energética y la protección de los consumidores ha traido consigo una número de modificaciones profundas en la regulación del autoconsumo en España. Entre ellas, una de las más interesantes para el consumidor es el denominado [**balance neto**](https://www.cambioenergetico.com/blog/balance-neto/).

Antes que nada, ubiquémonos. El RD 15/2018 determina dos modalidades de [**autoconsumo fotovoltaico**](https://www.cambioenergetico.com/autoconsumo/): sin excedentes, es decir, un tipo de autoconsumo destinado únicamente a cubrir las necesidades energéticas de quien produce la energía y que no está sujeto a ningún tipo de registro administrativo y con excedentes, que introduce un modelo, esta vez sí mediante registro, en el que quien produce la energía fotovoltaica puede inyectar el excedente que no utilice en la red eléctrica a cambio de una compensación. Es aquí donde se ubica el concepto de balance neto pero conviene que nos dentengamos un momento en este modelo denominado con excedentes para entender un matiz importante. 

## TIPOS DE AUTOCONSUMO SOLAR

El RD establece dos subgrupos de autoconsumo en el modelo con excedentes. En el grupo A se integran las instalaciones con las siguientes características: 

· Que la fuente de energía primaria sea de origen renovable. 
· Que la potencia de la instalación sea menor o igual a 100kW. 
· Que exista un único contrato de suministro con una empresa comercializadora.
· Que productor y consumidor suscriban un contrato para la compensación de excedentes.
· Que la instalación no esté sujeta a un régimen retributivo adicional o específico. 

En un segundo grupo -grupo B- también dentro del modelo de autoconsumo con excedentes irían simplemente todas aquellos casos que no cumplan alguno de los requisitos mencionados para el grupo A.

Antes hemos mencionado que el RD establece un mecanismo de compensación para la modalidad de **autoconsumo con vertido**. Pues bien, este mecanismo es distinto según se trate del grupo A o del grupo B dentro de esta modalidad. De este modo, para los productores del grupo A que lo soliciten se realizará una “*compensación simplificada entre los déficits y los excedentes de consumo*”. Dicho de forma sencilla, este tipo de compensación permitirá que un autoconsumidor vierta en la red un excedente de energía fotovoltaica que no necesite en un momento dado con la garantía de que podrá acudir posteriormente a la red eléctrica y recuperar esa misma cantidad de energía vertida con anterioridad, siempre que lo haga en el mismo periodo horario en que realizó el vertido en la red general. Este modelo de compensación en el que se cambia energía por energía es lo que denominamos balance neto. 

Cosa distinta es el sistema de compensación que se aplica para los autoconsumidores con excedentes del grupo B, que reciben una *contraprestación económica por el vertido a la red del excedente de energía de sus paneles solares*. En este caso, no se debe hablar tanto de balance neto como, simplemente, de una compensación acordada entre dos partes: el consumidor-productor y la compañía eléctrica. 

## ¿CÓMO TE BENEFICIA EL BALANCE NETO?

Lo que realmente es interesante desde el punto de vista del consumidor es que este tipo de compensaciones le ayudará a **amortizar la instalación solar más rápidamente**. En el caso del balance neto, la amortización ocurre porque el autoconsumidor podrá sacar más partido de la producción de su instalación fotovoltaica. Donde antes simplemente se perdía la producción solar que no se utilizaba, se pasa a un modelo en el que se puede “guardar para luego” en la red eléctrica. Como consecuencia, a partir de ahora habrá un mayor número de ocasiones en las que el consumidor acuda a la red convencional, no para consumir energía que luego habrá de pagar, sino para “recuperar” la energía vertida con anterioridad y que ahora necesita. Esto significa que el consumidor hará uso de un porcentaje mucho mayor de la energía que producen sus paneles solares y, por tanto, que podrá amortizar la inversión en su instalación fotovoltaica mucho antes.

Como se ha dicho, el modelo de autoconsumo con excedentes, que es el único para el que se aplican estos modelos de compensación, requerirá en su momento la inscripción en un registro que para tal fin crearán las distintas comunidades autónomas. Otra opción es realizar tu instalación fotovoltaica con Cambio Energético, en cuyo caso nosotros mismos nos encargamos de toda la gestión por ti.

A medida que el mercado de la energía fotovoltaica se sigue desarrollando, aumentan las razones para que los consumidores nos planteemos con seriedad la transición a la energía solar fotovoltaica. Contacta con Cambio Energético, nos encantará asesorarte sobre los beneficios de este modelo energético y sobre cómo hacer la transición al mismo de forma ordenada y sencilla.

www.cambioenergetico.com

