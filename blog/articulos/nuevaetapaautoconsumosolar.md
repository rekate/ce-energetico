## EL GOBIERNO APRUEBA EL BALANCE NETO

El Consejo de Ministros ha aprobado hoy la nueva normativa sobre el autoconsumo de energía en nuestro país: el Real decreto 15/2018. Se abre así un nuevo escenario legal que reconoce el autoconsumo como un derecho del ciudadano, ofreciendo a consumidores y empresas la posibilidad de producir su propia energía de autoconsumo, individual o colectivamente, sin tener que pagar ninguna tasa y con la opción de recibir una retribución por los excedentes que genere a través del denominado [**balance neto**](https://www.cambioenergetico.com/blog/balance-neto-y-autoconsumo-fotovoltaico/). 

La inclusón en el nuevo contexto legal del mecanismo de balance neto ofrece beneficios casi inmediatos para los consumidores. De un lado, posibilitará que aquellos que ya cuenten con un [*kit solar de autoconsumo*](https://www.cambioenergetico.com/60-kit-solar-para-autoconsumo) o cualquier otro tipo de instalación de [**placas solares**](https://www.cambioenergetico.com/171-placas-solares) en su empresa o en su residencia puedan recuperar su inversión con más rapidez, siempre que se adhieren a alguna de las modalidades de [*autoconsumo con excedentes*](https://www.cambioenergetico.com/blog/el-balance-neto-sera-inminente/). De otro lado, el balance neto, unido a otras novedades como la posibilidad del autoconsumo compartido y la simplificación de los procesos supone una motivación extra para que aquellos consumidores que aún dependen de la electricidad convencional se animen a invertir en placas solares para producir su propia energía y ser así más independientes de las compañías eléctricas. 

## ¿Qué es el balance neto?

En esencia, el balance neto consiste en una compensación ofrecida al propietario de una instalación de autoconsumo fotovoltaico por verter a la red eléctrica el excedente de energía que le sobra. 

## ¿Todos los tipos de compensación que ofrece la nueva legislación son balance neto?

No. El mecanismo de retribución es distinto dependiendo de la modalidad de autoconsumo a la que se adhiera el consumidor. Veámoslo con detalle. 
El [*Real Decreto 15/2018*](https://www.cambioenergetico.com/blog/claves-del-real-decreto-ley-15-2018-de-autoconsumo/) aprobado hoy establece dos modalidades de autoconsumo fotovoltaico: sin excedentes, es decir, destinado únicamente a cubrir las necesidades energéticas de quien produce la energía y con excedentes, en la que quien produce la energía fotovoltaica puede “vender” la producción que le sobra inyectándola en la red eléctrica a cambio de una compensación. Se establecen asimismo dos subgrupos de autoconsumo en el modelo con excedentes. El grupo A integra instalaciones que, entre otros requisitos, tienen como fuente de energía primaria la renovable y una potencia menor o igual a 100kW. El grupo B integra al resto de instalaciones.

El balance neto es el tipo de compensación destinada a los consumidores o empresas del grupo A y podría describirse como “*energía por energía*”. Los autoconsumidores de este grupo tienen la posibilidad de verter en la red el excedente de energía fotovoltaica que no necesiten en un momento dado con la garantía de que podrán acudir posteriormente a la red eléctrica y recuperar esa misma cantidad de energía, siempre que lo hagan en el mismo periodo horario durante el que se realizó el vertido en la red. El resultado es menos dependencia de la red eléctrica convencional, más consumo de la energía producida por los paneles solares y, por tanto, más **ahorro en la factura de la luz**.

> El balance neto es el tipo de compensación destinada a los consumidores y empresas - Cambio Energético

Para consumidores o empresas que se adhieran al grupo B, también existe una compensación, si bien no puede considerarse estrictamente como balance neto. En este caso, la compensación consiste en “*energía por contraprestación económica*”, es decir, el autoconsumidor recibe una retribución dineraria por su excedente de energía. El valor de la energía vertida se estipula a precio horario de mercado deduciendo los costes de comercialización, dejando en manos de las comercializadoras eléctricas el cálculo de estas cantidades.  

Con la introducción de medidas de compensación como [*el balance neto*](https://www.cambioenergetico.com/blog/balance-neto/), la simplificación de los procesos o la aprobación del autoconsumo compartido, largamente esperada, el Real decreto 15/2018 deja atrás un marco legal que penalizaba el autoconsumo energético con renovables para pasar a incentivarlo, equiparando así la normativa española a la de otros países europeos y situando a nuestro país en una mejor posición de cara al cumplimiento de los objetivos de energías renovables establecidos para 2030.

Es el mejor momento para hacer la **transición a la energía fotovoltaica**. Si estás pensando en *invertir en tu instalación de autoconsumo*, no dudes en contactar con nosotros para que te hagamos un presupuesto sin compromiso. Y una vez que te decidas, no tienes que preocuparte de nada, nosotros nos haremos cargo de todos los procesos necesarios para legalizar tu instalación. 

Da el paso a la energía fotovoltaica.

www.cambioenergetico.com 

