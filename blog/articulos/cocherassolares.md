# POR QUÉ VAS A QUERER INSTALAR UNA COCHERA SOLAR

Una de las grandes **ventajas que ofrece la energía fotovoltaica** es la de convertir cualquier espacio de nuestra casa o empresa en un punto de producción eléctrica. Los aparcamientos y marquesinas son un buen ejemplo: excelentes para producir sombra y proteger de la radiación solar, ¿por qué no darles una función más y dejar que aprovechen esa radiación para producir electricidad limpia y gratuita?


## ¿Qué es un aparcamieto o cochera solar?

Un aparcamiento solar es una estructura de **parking con placas solares** a modo de cubierta. Como cualquier otra cochera, protege el vehículo de las inclemencias del tiempo y da sombra en días de calor pero ofrece una funcionalidad que la distingue claramente de cualquier otro modelo convencional: su cubierta de paneles solares puede generar electricidad fotovoltaica, bien para almacenarla en un sistema de baterías o para [autoconsumo fovotoltaico](https://www.cambioenergetico.com/blog/autoconsumo-fotovoltaico-guia-completa/) en residencias o industria, con el consiguiente ahorro que ello supone para la factura de la luz.

Un **aparcamiento solar** se parece a cualquier otra estructura de placas solares sobre suelo en tanto que tampoco necesita una superficie pre-existente sobre la que instalarse. Eso sí, son generalmente son mucho más altas –lo suficiente para poder albergar un vehículo- y tienen un número de ventajas propias, tanto para uso residencial como industrial, que van más allá del ahorro energético. Vamos a echarles un vistazo. 


## Aparcamiento solar para residencia. Parking solar para industria

La estructura de una cochera solar suele consistir en módulos autónomos de acero galvanizado y perfiles de aluminio para fijar las placas. Esto significa que pueden instalarse en serie, uniendo tantos módulos como pueda albergar el espacio destinado a aparcamiento. ¡Y a veces puede albergar mucho! Basta con echar un ojo al complejo que se ha inaugurado hace solo unos días en Corbas (Francia) y que es el mayor **parking solar** hasta la fecha: más de 12 hectáreas de superficie de parking que albergan 4600 plazas de aparcamiento y que producirán 16.3 MW de energía. 

Récords aparte, lo interesante es la flexibilidad que ofrece este tipo de estructura, lo que la hace apta tanto para un aparcamiento en una vivienda con uno o dos vehículos como para el parking de una gran superficie industrial. 


## Angulación y orientación “A la carta”

No existe una única manera de disponer las [**placas solares**](https://www.cambioenergetico.com/26-paneles-solares) de un aparcamiento solar. Estos pueden estar angulados (ver imagen abajo) de forma ascendente o estar dispuestos casi en plano, a veces con una ligera curvatura en los extremos buscando albergar el máximo número de vehículos posible. 

Sea como fuere la disposición final de los paneles,  un aparcamiento solar simplemente se desentiende de cualquier limitación en lo concerniente a angulación, orientación o tamaño del sistema de paneles. Es habitual que las cubiertas de residencias o naves industriales ofrezcan ciertas limitaciones provocadas por la propia arquitectura de los tejados. A veces, el espacio resulta insuficiente para instalar la potencia deseada. Otras veces, la orientación de la cubierta no es la adecuada o presenta obstáculos arquitectónicos que hacen que la instalación no siempre pueda ser óptima. Los aparcamientos solares, al no ir sujetos a ningún tejado o estructura pre-existente, puede tener el nivel de angulación y orientación de las placas solares y el tamaño que más convenga al usuario. Lo dicho: energía fotovoltaica “à la carte”.


## Un coche hoy. Dos mañana. Aparcamiento solar 

La instalación de aparcamientos solares es completamente escalable y puede, por tanto, ajustarse progresivamente a las necesidades cambiantes de cualquier usuario residencial o industrial. Esta facultad no es consecuencia exclusiva del carácter modular de estas estructuras. Simplemente, el proceso de instalación en sí dista de ser un procedimiento largo y complejo. Las estructuras de parking solar son resistentes pero muy ligeras, con lo que se instalan fácilmente sin necesidad de soldaduras, maquinaria pesada ni trabajo difícil. Del mismo modo, Cambio Energético comercializa un modelo en formato kit (link a tienda), en el que las estructuras se sirven con todos los componentes premontados y con un manual de instalación paso a paso para el usuario que prefiera instalarlo él mismo.  



## Compañera ideal de la tecnología fotovoltaica bifacial

La productividad de una instalación de cochera solar es similar a la de otro tipo de instalaciones fotovoltaicas como las habituales en tejados y estructuras de suelo. No en vano, una estructura de **aparcamiento solar** acepta prácticamente cualquiera de los modelos de panel solar que existen hoy en día en el mercado y, de hecho, son estructuras especialmente indicadas para albergar uno de los tipos de panel solar más tecnológicamente avanzado y que está empezando a generalizarse en el mercado, el denominado [**panel bifacial**](https://www.cambioenergetico.com/blog/ultima-tecnologia-placas-solares).

Los paneles bifaciales son “dobles”, es decir, cuentan con células fotovoltaicas en sus dos caras, de manera que son capaces de producir energía solar por ambas, con un lógico incremento en la producción de energía de la instalación, que se incrementa con el nivel de angulación, orientación y altura que una estructura de cochera solar está en perfectas condiciones de ofrecer. 


## Parking solar

Terminamos este post describiendo dos propiedades del aparcamiento solar especialmente interesantes desde el punto de vista de la industria. La primera de ellas es la posibilidad de integrar en la estructura otros elementos como luces o incluso cargadores de vehículos eléctricos. Esta capacidad es especialmente interesante para la creación de electrolineras (link al artículo sobre cargar el vehículo eléctrico con paneles), es decir, puntos de carga de coches eléctricos que podrían ubicarse con facilidad en gasolineras, establecimientos turísticos, polígonos industriales y superficies comerciales de todo tipo. 

La segunda propiedad que consigue una empresa que invierte en parking solar es que este tipo de tecnología es un alegato en pro de la sostenibilidad que se reflejará en una mejor imagen de empresa. Los consumidores, cada vez más concienciados de la necesidad de proteger el medioambiente, recompensarán a la empresa que demuestre con hechos y no palabras su compromiso de responsabilidad para con el planeta. 

A medida que el mercado de la energía fotovoltaica sigue introduciendo novedades, aumentan las razones para que los consumidores nos planteemos con seriedad la transición a la energía solar fotovoltaica. Cambio Energético te ofrece tu instalación de aparcamiento solar con componentes de los mejores fabricantes y toda la garantía de nuestra red de delegaciones y servicios técnicos en todo el territorio nacional. Podrás, además, financiar tu inversión hasta en 36 meses sin intereses.

Da el paso a la energía fotovoltaica.

www.cambioenergetico.com 
