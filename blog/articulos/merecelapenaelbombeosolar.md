# EN EL CAMPO, ¿MERECE LA PENA EL BOMBEO SOLAR?

Cada vez con más frecuencia, explotaciones agrícolas y ganaderas de todo tipo están apostando por las [**bombas solares de riego**](https://www.cambioenergetico.com/190-bombas-solares-sumergibles) como una alternativa limpia y eficiente frente al uso de generadores eléctricos basados en combustibles fósiles. Detrás de este cambio de mentalidad se encuentra, sin duda, la continua subida del precio de la electricidad convencional y una mayor concienciación de la necesidad de luchar contra el Cambio Climático, pero también un número de ventajas que hacen del bombeo solar un verdadero optimizador del trabajo en el campo. En este post os contamos esas ventajas.

## ¿Qué es bombeo solar?

Antes de meternos en detalles, especifiquemos a qué nos referimos con bombeo solar. En esencia, el bombeo solar no se diferencia mucho de un bombeo convencional. Ambos sistemas están ideados para extraer agua -normalmente desde un pozo- a través de una bomba hidráulica. Esa agua luego puede distribuirse para diversos usos: riego, abrevaderos para ganado, agua potable, etc. La diferencia esencial entre ambas modalidades es la fuente de energía que se utiliza para hacer funcionar la bomba. Así, el bombeo tradicional utiliza la red eléctrica o un generador de diésel o gasolina si la localización no cuenta con acceso a la red. En el caso del bombeo solar, la fuente de energía es una [**instalación de paneles solares**](https://www.cambioenergetico.com/171-placas-solares).

> Invertir en un sistema de bombeo solar es invertir en quitarse problemas.  — Cambio Energético

## Componentes de un sistema de bombeo solar

Un sistema de [**bombeo solar**](https://www.cambioenergetico.com/111-kit-solar) nos permite hacer todo lo que nos permite un sistema de riego tradicional. En los últimos años, las soluciones de bombeo solar han evolucionado bastante, incorporando nuevos componentes para optimizar el control sobre todo el proceso y su eficiencia: sensores de pozo seco, sensores de control de estanque, de radiación, control horario de los sistemas, funciones remotas… La configuración final del sistema dependerá siempre de factores muy variados, desde la profundidad a la que se encuentra el agua que se quiere extraer a la pureza de la misma, la distancia que la separa del depósito de almacenamiento o el caudal que se necesita según el sistema de distribución establecido (riego por goteo, aspersión, almacenamiento, etc.).

## En general, una instalación de bombeo solar cuenta con los siguientes elementos básicos: 

Fuente de alimentación: Un sistema de [**placas solares**](https://www.cambioenergetico.com/171-placas-solares) bien dimensionado puede cubrir las necesidades de electricidad de cualquier instalación de bombeo. Es aconsejable, no obstante, contar con algún sistema de backup de energía -por ej. un generador- si se suceden días de poca producción solar o si no se cuenta con un sistema de almacenamiento de agua.

### Bomba solar: 
Puede ser tanto de corriente continua como de alterna monofásica y trifásica. De acuerdo con sus aplicaciones, hay tres tipos fundamentales: sumergibles, de superficie y de piscinas (de estas últimas ya hablaremos en futuros posts). 
- [**bomba sumergible**](https://www.cambioenergetico.com/190-bombas-solares-sumergibles): su gran capacidad de aspersión la hace especialmente indicada para bombear agua localizada a gran profundidad (puede llegar a más de 100m). Suele tener un diseño de cilindro alargado.
- [**bomba de superficie**](https://www.cambioenergetico.com/189-bombas-solares-de-superficie): más indicada para *bombear agua* desde un pozo o balsa a altura media. 
Es capaz de crear un caudal de mucho volumen, de manera que puede distribuir agua a una zona de gran superficie. 

### Variador de frecuencia o Controlador : 
Ambas son piezas centrales en una instalación de bombeo solar, ya que gestionan la energía que captan los paneles. La necesidad de uno u otro dependerá de si existe o no una instalación de bombeo previa: 

- Si se está renovando un sistema de bombeo tradicional para convertirlo en solar, lo normal es que ya se cuente con una bomba, aunque no específicamente solar. Si no se pretende cambiar el tipo de bomba, será necesario un [**variador de frecuencia**](https://www.cambioenergetico.com/202-variadores-de-frecuencia) que convierta la energía de los paneles en energía “usable” por dicha bomba.

- Si se desea sustituir la bomba convencional por una solar o si lo que se pretende es crear directamente un **sistema de bombeo solar desde cero que ya incluya bomba solar, lo que será necesario es un controlador que gestione, además de la energía de los paneles, las operaciones del sistema, analizando la potencia disponible y regulando el trabajo de la bomba, de manera que se obtenga el mayor caudal de agua posible en todo momento. 

## Red de distribución: que lleve el agua extraída a los puntos de riego, uso doméstico, ganadería, etc.

Estos elementos que hemos denominado básicos pueden complementarse con otros componentes:

### Sistema de almacenamiento
Generalmente es un depósito. *Garantiza la demanda de agua durante periodos de baja producción de energía solar* (por ej. días nublados, noche, etc.) sin necesidad de acudir a generadores o a la red eléctrica. 

### Sistema de monitorización y gestión
Algunos fabricantes como **Lorentz o Grundfos** incluyen en sus soluciones sistemas de monitorización y gestión remota de las instalaciones de bombeo solar a través de Internet. Estos sistemas -que suelen incluir Apps para smartphones- permiten configurar la instalación, automatizar el funcionamiento del sistema, acceder a información sobre el estado del sistema en tiempo virtualmente real (la información suele actualizarse cada pocos minutos), comprobar la eficiencia del sistema en un periodo determinado de tiempo mediante un repositorio histórico de datos y estar informado ante cualquier incidencia técnica. 

## Ventajas del bombeo solar

*El bombeo solar aporta un número de ventajas a la gestión de las explotaciones agrícolas y ganaderas*. Estas ventajas se resumen en un incremento del ahorro, la operatividad, la productividad del suelo y la eficiencia, amén de la ausencia de emisiones contaminantes. El resultado es un aumento de la competitividad de la explotación considerable.

#### Ahorro
Una de las grandes [**ventajas del bombeo solar**](https://www.cambioenergetico.com/blog/bombeo-solar-ventajas/) es la reducción de costes que supone para el agricultor o ganadero. En primer lugar, porque toda energía que produzcan las placas solares del sistema es energía que se elimina de la factura de la luz. El bombeo solar, además, elimina los costes en gasoil o gasolina asociados a los generadores eléctricos y no requiere de baterías (cuyo rol asume en este caso el depósito de almacenamiento). Como se ha dicho más arriba, es aconsejable mantener un generador eléctrico pero sólo a modo de respaldo si se suceden varios días de poca producción solar que no puedan compensarse con el agua almacenada en el depósito. 

#### Operatividad 
Dado que no requieren de red eléctrica para su funcionamiento, los sistemas de **riego solar** pueden instalarse literalmente en cualquier localización sin que esto suponga la más mínima pérdida de tiempo para el agricultor o ganadero. Como hemos visto, los sistemas actuales permiten monitorizar y automatizar los procesos de forma remota a través de Internet, de manera que se evita tener que desplazarse hasta la localización del pozo y gastar tiempo en comprobaciones in situ. 

#### Mayor eficiencia en el riego 
El riego solar se caracteriza por su capacidad de mantener un nivel de extracción de agua -y por tanto un nivel del caudal- constante, y esta capacidad ofrece mayor eficiencia en dos frentes. De un lado, **aumenta la eficiencia del riego** y por tanto la productividad del suelo. De otro lado, esa misma capacidad de mantener constante el nivel de extracción de agua provoca que el *bombeo solar sea también especialmente efectivo en pozos con poco poder de recuperación*.

#### Nulo mantenimiento o incidencias 
Al contrario de lo que suele ocurrir con generadores eléctricos de gasolina o diésel usados en el bombeo convencional, el bombeo solar requiere muy poco mantenimiento. Si a esto se añade el hecho de que los equipos fotovoltaicos tienen una excelente durabilidad -las garantías de los paneles solares llegan a los 25 años- puede concluirse que **invertir en un sistema de bombeo solar es invertir en quitarse de problemas**. Simplemente. 

#### Cero emisiones 
Trabajar con **energías renovables en el medio rural** significa, más allá de las ventajas vistas arriba, una más importante: la de garantizar la sostenibilidad de los recursos del campo y, con ello, el futuro de las generaciones que habrán de trabajar en él. La energía solar es 100% limpia y tan inagotable como el mismo sol. Sin emisiones ni *huella de carbono*. Pura garantía de futuro.
 
**Cambio Energético** es una empresa comprometida con el desarrollo sostenible del medio rural. Como especialistas en sistemas de bombeo solar, sólo trabajamos con los mejores fabricantes de equipos y componentes y damos un servicio integral, desde el estudio de viabilidad del proyecto a la instalación del último componente y todo un servicio de soporte al cliente que se extiende a lo largo de la totalidad de la vida útil de la instalación. 
 
Da el paso a la **energía fotovoltaica**. Confía en Cambio Energético.

www.cambioenergetico.com
