# HAY RAZONES INDUSTRIALES PARA PASARSE A LA ENERGÍA FOTOVOLTAICA 

Con un abanico de ventajas que van desde el **ahorro energético** a la desgravación fiscal o al escaso mantenimiento de las placas solares, las instalaciones de energía fotovoltaica industrial se están revelando como una herramienta fundamental para hacer más competitivas a empresas de todos los sectores.

Y en este post queremos hablar, precisamente, de todas esas ventajas. Haz números y verás que si aún no has dado el salto a la *energía solar* para tu empresa, ya estás tardando. 

Veámoslo todo con detalle. 

## Flexibilidad energética 

Da igual que se trate de una empresa de 5 empleados o de 250. De producción o de servicios. Dedicada a la alimentación o al diseño aeroespacial. En un municipio con conexión a la red eléctrica o en mitad de la nada, [viviendas aisladas de la red eléctrica](https://www.cambioenergetico.com/148-kits-para-vivienda-aislada). Da igual. La gran variedad de configuraciones posibles que ofrece una **instalación de energía solar** hace que esta tecnología tenga una extraordinaria capacidad de aplicarse a todo tipo de labores industriales, sin afectar a los procesos ni crear incompatibilidades, sea cual sea el sector de actividad. [Granjas avícolas](https://www.cambioenergetico.com/blog/el-futuro-de-las-granjas-avicolas-es-solar), gasolineras, [distribuidoras de alimentos](https://www.cambioenergetico.com/blog/autoconsumo-industrial-congelados-gran-canaria/), [explotaciones de regadío](https://www.cambioenergetico.com/blog/galeria/bombeo-solar/#.XJnXIyhKiUk), [empresas de infraestructura urbanas](https://www.cambioenergetico.com/blog/farolas-solares-en-granada/)… Nuestra propia experiencia nos ha enseñado que la *energía fotovoltaica aplicada a la industria* no discrimina sectores. Es, simplemente, una fuente más inteligente, barata y limpia de obtener la electricidad que se necesita. 

Pensemos en el proceso de [**instalación de placas solares**](https://www.cambioenergetico.com/111-kit-solar) en una típica nave industrial. Muy al contrario de lo que pueda pensarse, se trata de un procedimiento relativamente rápido –unos pocos días como máximo- que normalmente tiene lugar en la cubierta de la nave. Es un proceso, por tanto, que no altera significativamente el proceso de producción habitual de la empresa. Además, el hecho de que la instalación de paneles solares se ubique en la cubierta hace que, en la enorme mayoría de los casos, la instalación final no reste ningún espacio útil de trabajo en la nave. Es más, puede que convierta en productiva una zona que probablemente no lo era hasta entonces. 

Una empresa que opta por **producir su propia energía con paneles solares** no requiere de ningún procedimiento específico o de aprender nada nuevo por tratarse de una instalación fotovoltaica. Los trabajadores simplemente pueden seguir trabajando como hasta entonces, salvo que ahora estarán *ahorrando en la factura de la luz* y eliminando la huella de carbono de su actividad.

## Ahorro energético

Una instalación de [**placas solares**](https://www.cambioenergetico.com/171-placas-solares) en pleno funcionamiento puede conseguir que la empresa pague su electricidad virtualmente a mitad de precio. Por supuesto, la producción de una instalación fotovoltaica varía según la estación y la climatología en general pero, en cualquier caso, la reducción media en la factura de la luz puede situarse en torno al 40-45% mensual, más aún si se utilizan baterías de almacenamiento, algo muy común en instalaciones fotovoltaicas para empresa. 

Se trata, sin duda, de una cantidad considerable pero que resulta fácil de entender si se tiene en cuenta que, por lo general, el periodo de mayor actividad de una empresa –y por tanto el periodo con mayores picos de consumo de energía- se dan durante las horas centrales del día, justo en el periodo de la jornada en el que las placas solares reciben la máxima cantidad de luz solar. Dicho de otro modo: cuando más electricidad requiere la actividad empresarial, mayor es la cantidad de *energía solar que producen los paneles de la instalación fotovoltaica*. Pura eficiencia energética.

Merece la pena pararse a pensar en la reducción de costes estructurales que un ahorro como el que estamos describiendo puede suponer para la empresa. Se trata de un beneficio que redunda directamente en su competitividad, permitiendo, por ejemplo, una política de precios más agresiva con respecto a la competencia. 

El **ahorro fotovoltaico** es, asímismo, cuantificable desde el primer día. La mayoría de fabricantes de tecnología fotovoltaica ofrecen en sus equipos sistemas para monitorizar, en tiempo real, tanto el consumo de electricidad que está generando la actividad de la empresa como la producción eléctrica de sus placas solares. Esto permite al empresario conocer exactamente qué porcentaje de las necesidades de energía de su empresa está siendo proporcionado por sus placas solares en cualquier momento de la jornada. 

## Amortización de la instalación fotovoltaica

Aunque fundamental, el *ahorro en la factura de la luz* no es el único beneficio que la [**energía fotovoltaica industrial**](https://www.cambioenergetico.com/autoconsumo-industrial/) puede ofrecer a la cuenta de resultados del empresario. Nuestra experiencia nos ha enseñado que una empresa tarda una media de entre tres y siete años en recuperar la inversión realizada en su instalación fotovoltaica. Este tiempo se convierte en casi nada si tenemos en cuenta la enorme durabilidad de este tipo de equipamiento –con garantías de hasta 25 años en la mayoría de fabricantes - y el poco mantenimiento que requiere. Esto significa que, una vez la instalación ha sido amortizada, ésta ofrecerá aún a la empresa décadas de servicio. Difícil poder encontrar otro producto con la misma calidad-precio. 

## El valor del excedente de energía: balance neto

La nueva legislación [RD 15/2018](https://www.cambioenergetico.com/blog/claves-del-real-decreto-ley-15-2018-de-autoconsumo/) que, desde el pasado mes de octubre, regula el sector de la energía fotovoltaica en nuestro país ha introducido un sistema de compensación de los excedentes de energía que, sin duda, es una razón más para dar el salto a la energía fotovoltaica industrial. Se trata del concepto de [**balance neto**](https://www.cambioenergetico.com/blog/balance-neto/). 

La ley determina actualmente dos modalidades de [**autoconsumo fotovoltaico**](https://www.cambioenergetico.com/autoconsumo/): sin excedentes, es decir, destinado únicamente a cubrir las necesidades energéticas de quien produce la energía y con excedentes, en la que quien produce la energía fotovoltaica puede inyectar la producción que le sobra en la red eléctrica a cambio de una compensación. Este último caso no es ya, por tanto, de autoconsumo en sentido estricto, dado que quien produce su propia energía puede además “hacer negocio” con la que le sobra. 

Dentro de este grupo con excedentes se establecen dos subgrupos. El grupo A lo integran instalaciones que, entre otros requisitos, tienen como fuente de energía primaria la renovable y una potencia menor o igual a 100kW (una *instalación solar industrial* media ronda los 30-50kW). En un segundo grupo -grupo B- irían simplemente todas aquellos casos que no cumplan los requisitos del grupo A. 

Lo interesante es que, para los productores del grupo A que lo soliciten, la ley permite una “compensación simplificada entre los déficits y los excedentes de consumo”. Dicho de otro modo, una empresa puede vertir en la red un excedente de energía fotovoltaica que no necesite en un momento dado con la garantía de que podrá acudir posteriormente a la red eléctrica y recuperar esa misma cantidad de energía, siempre que lo haga en el mismo periodo horario en que realizó el vertido en la red. Este intercambio de energía por energía es el mecanismo de compensación que se denomina *balance neto* y que, como puede entenderse, permite al empresario acelerar el proceso de **amortización de su instalación fotovoltaica**. Para los productores del grupo B también existe una compensación, en este caso económica, por inyectar a la red el excedente de energía de sus [paneles solares](https://www.cambioenergetico.com/26-paneles-solares). 

## Una actividad sin tasas y con ventajas fiscales, energía solar para todos

Gracias a la [**nueva legislación del sector fotovoltaico**](https://www.cambioenergetico.com/blog/claves-del-real-decreto-ley-15-2018-de-autoconsumo/), hemos pasado de un escenario en el que la decisión de apostar por el autoconsumo energético significaba enfrentarse a tasas y procesos burocráticos desalentadores a un escenario nuevo donde no sólo no existen tasas al autoconsumo, sino que se fomenta la actividad a través de un número de [ventajas fiscales](https://www.cambioenergetico.com/blog/bonificaciones-fiscales-para-el-autoconsumo-energetico/). Tasas como el Impuesto de Bienes Inmuebles (IBI) o el Impuesto sobre Instalaciones, Construcciones y Obras (ICIO) son un buen ejemplo. Aunque hay importantes diferencias entre municipios y comunidades autónomas, una mayoría de núcleos de población importantes del país están apostando bonificar el IBI para autoconsumidores de energía en una media de un 50% con una duración variable que ronda los 3-5 años, según municipios. Este porcentaje alcanza el 90-95% en el caso del ICIO. Sin duda, más buenas noticias sobre la energía fotovoltaica industrial.

## Energía fotovoltaica sin inversión inicial: Modalidad de Inversión a Coste Cero

El mercado de la electricidad global lleva ya algunos años siendo testigo de una doble tendencia. Por un lado, los precios de la electricidad convencional con origen en combustibles fósiles siguen aumentando a marchas forzadas. De otro, el precio de los equipos de *autoconsumo solar* bajan a velocidad de vértigo, merced a la innovación tecnológica y al número creciente de consumidores comprometidos con los **beneficios de la energía solar**. 

A pesar de este escenario favorable, sigue habiendo empresarios que aún no se han decidido a invertir en una instalación fotovoltaica para sus negocios. Pues bien, a estos empresarios les decimos: ¿Quién dice que haya que invertir? Cambio Energético dispone de [**energía solar a coste cero**](https://www.cambioenergetico.com/blog/tu-instalacion-fotovoltaica-sin-gastar-un-solo-euro/).

En la modalidad de Inversión a Coste Cero es Cambio Energético quien se hace cargo del 100% de los costes de instalar el *sistema de paneles solares*, así como del mantenimiento de la instalación. El cliente sólo tiene que estar dispuesto a repartir con nosotros los beneficios del ahorro energético que obtenga.

La modalidad de Inversión a Coste Cero es un producto muy flexible, tanto en su modo de facturar como en la propia duración del contrato, y sigue un patrón escalable: durante los primeros años de actividad de las placas solares, el ahorro en electricidad generado por las mismas se repartirá a razón de un 75% destinado a la amortización de la instalación fotovoltaica y un 25% a repartir equitativamente entre el cliente y Cambio Energético. 

Una vez ya se hayan recuperado los costes de la instalación, el reparto ascenderá a un 50% para la empresa cliente y un 50% para Cambio Energético. 

Pasados ocho o nueve años desde el inicio del servicio, la empresa puede optar por comprar la [instalación solar](https://www.cambioenergetico.com/kits-solares/) a un precio que será ya mucho menor o , simplemente, desmantelarla.

## Movilidad solar

Aunque no goce de muchos minutos de publicidad televisiva, lo cierto es que el vehículo comercial eléctrico es una realidad que está proliferando con tanta o más rapidez que otros vehículos eléctricos convencionales. Tanto es así que [gasolineras](https://www.cambioenergetico.com/blog/autoconsumo-fotovoltaico-gasolinera-huelva) o talleres de toda España se están dando prisa en implimentar cargadores de este tipo de vehículos en sus instalaciones. Para este tipo de empresas, la decisión de contar con una instalación de placas solares que suministre energía a sus cargadores es una garantía de rentabilidad, ya que supone la diferencia entre poder ofrecer este servicio con un 100% de margen comercial o incurrir –si se recurre a la electricidad convencional- en costes que reduzcan dicho margen. 

Para una mayoría de otras empresas no relacionadas con la automoción, el vehículo es principalmente un medio de reparto o de transporte, pero aquí también hay posibilidad de ahorrar en costes. Invertir en una flota de [**vehículos comerciales eléctricos**](https://www.cambioenergetico.com/blog/puedo-recargar-mi-coche-electrico-con-paneles-solares/) alimentados con la producción de una instalación fotovoltaica es un modo eficaz de ahorrar gastos en combustible. Los fabricantes de equipos fotovoltaicos han visto las posibilidades de esta opción y no han dudado en empezar a ofrecer ya cargadores de coche eléctrico en sus soluciones de energía solar. 

## Energía limpia. Industria limpia.

No es una exageración afirmar que consumidores y empresas por igual hemos perdido el derecho a frivolizar con el Medioambiente. 

Toda actividad industrial que obtiene su energía de una **instalación de placas solares elimina su huella de carbono**. Así de simple. 

Puede que no esté expresada en términos económicos pero apostar por la sostenibilidad es un beneficio real y tangible para la empresa. No sólo porque garantiza el futuro de su actividad industrial y de las generaciones que han de ganarse la vida con ella en el futuro. También porque apostar por la energía fotovoltaica es apostar por la imagen de la empresa ante un público cada vez más concienciado de la necesidad de proteger el planeta. Este público no dudará en recompensar a la empresa en sus decisiones de compra y, esto sí, es perfectamente cuantificable. Y es que no hay mejor estrategia de promoción de marca que hacer las cosas correctamente. 

El futuro de la industria es solar. Contacta con **Cambio Energético** y te asesoraremos sin compromiso sobre las posibilidades que la energía fotovoltaica ofrece para tu negocio. 

¡Da el paso!

www.cambioenergetico.com
